<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Client
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Job[] $jobs
 * @mixin \Eloquent
 */
class Client extends Model
{
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}
