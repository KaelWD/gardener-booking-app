<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJob extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'             => ['required', 'date'],
            'client'           => ['required', 'integer'],
            'useClientAddress' => 'boolean',
            'address'          => 'required_if:useClientAddress,false',
            'suburb'           => 'required_if:useClientAddress,false',
            'postcode'         => 'required_if:useClientAddress,false',
            'details'          => '',
            'price'            => ['required', 'string', 'regex:/^\d+(\.\d{2})?$|^(?!.)/'],
            'paid'             => 'boolean',
        ];
    }
}
