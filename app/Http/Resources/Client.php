<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class Client extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => (int) $this->id,
            'name'     => (string) $this->name,
            'address'  => (string) $this->address,
            'suburb'   => (string) $this->suburb,
            'postcode' => (string) $this->postcode,
            'jobs'     => $this->transformJobs($this->whenLoaded('jobs')),
        ];
    }

    protected function transformJobs($jobs)
    {
        return new class($jobs) extends ResourceCollection
        {
            public function toArray($request)
            {
                return $this->resource->map(function (\App\Job $job) {
                    return [
                        'id'        => $job->id,
                        'completed' => (bool) $job->completed,
                    ];
                });
            }
        };
    }
}
