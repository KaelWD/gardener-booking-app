<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Job extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'client_id'    => $this->client_id,
            'date'         => $this->date,
            'address'      => $this->address,
            'suburb'       => $this->suburb,
            'postcode'     => $this->postcode,
            'hours_worked' => $this->hours_worked,
            'details'      => $this->details,
            'completed'    => (bool) $this->completed,
            'price'        => $this->price,
            'paid'         => (bool) $this->paid,
        ];
    }
}
