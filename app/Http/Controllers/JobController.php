<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreJob;
use App\Job;
use Illuminate\Http\Request;
use App\Http\Resources\Job as JobResource;

class JobController extends Controller
{
    public function index()
    {
        return JobResource::collection(
            Job::orderBy('completed')
               ->orderBy('date')
               ->orderByDesc('price_cents')
               ->get()
        );
    }

    public function store(StoreJob $request)
    {
        $job = new Job();
        $job->fill([
            'client_id' => $request->client,
            'date'      => $request->date,
            'address'   => $request->address,
            'suburb'    => $request->suburb,
            'postcode'  => $request->postcode,
            // 'hours_worked' => '', // TODO
            'details'   => $request->details,
            // 'completed'    => '', // TODO
            'price'     => $request->price,
            'paid'      => $request->paid,
        ]);

        $success = $job->save();

        return (new JobResource($job))->additional(compact('success'));
    }

    public function replace(StoreJob $request, Job $job)
    {
        $job->fill([
            'client_id'    => $request->client,
            'date'         => $request->date,
            'address'      => $request->address,
            'suburb'       => $request->suburb,
            'postcode'     => $request->postcode,
            // 'hours_worked' => '', // TODO
            'details'      => $request->details,
            // 'completed'    => '', // TODO
            'price'        => $request->price,
            'paid'         => $request->paid,
        ]);

        $success = $job->save();

        return (new JobResource($job))->additional(compact('success'));
    }

    public function update(Request $request, Job $job)
    {
        $success = $job->fill($request->all())->save();

        return (new JobResource($job))->additional(compact('success'));
    }
}
