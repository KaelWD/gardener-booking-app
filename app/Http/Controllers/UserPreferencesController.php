<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserPreferencesController extends Controller
{
    public function index()
    {
        return response()->json(['data' => [
            'darkTheme' => false,
        ]]);
    }
}
