<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use App\Http\Resources\Client as ClientResource;

class ClientController extends Controller
{
    public function index()
    {
        return ClientResource::collection(
            Client::with('jobs')->get()
        );
    }
}
