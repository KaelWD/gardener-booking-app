<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

/**
 * App\Job
 *
 * @property-read \App\Client $client
 * @mixin \Eloquent
 */
class Job extends Model
{
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function getPriceAttribute()
    {
        $price = $this->attributes['price_cents'];
        $length = strlen($price);
        $dollars = $length > 2 ? substr($price, 0, -2) : '0';
        $cents = $length > 1 ? substr($price, -2) : '0' . $price;

        return "$dollars.$cents";
    }

    public function setPriceAttribute($price)
    {
        $splitPrice = explode('.', $price);

        $dollars = $splitPrice[0];
        $cents = count($splitPrice) > 1 ? $splitPrice[1] : 0;

        $this->attributes['price_cents'] = $dollars * 100 + $cents;
    }
}
