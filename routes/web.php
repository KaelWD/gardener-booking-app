<?php

Route::get('online-check', function() {
    return response()->json(['online' => true]);
});

Route::view('/', 'app');
