<?php

use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->group(function () {
    Route::get('user', function (Request $request) {
        return $request->user();
    });
});

Route::get('clients', 'ClientController@index');

Route::get('jobs', 'JobController@index');
Route::post('jobs', 'JobController@store');
Route::put('jobs/{job}', 'JobController@replace');
Route::patch('jobs/{job}', 'JobController@update');

Route::get('user/preferences', 'UserPreferencesController@index');

Route::get('{catchall?}', function() {
    throw new NotFoundHttpException();
})->where(['catchall' => '.*']);
