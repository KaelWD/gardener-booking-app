<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->unsigned()->nullable();
            $table->date('date');

            // If null use address details from clients table
            $table->string('address')->nullable();
            $table->string('suburb')->nullable();
            $table->string('postcode')->nullable();

            $table->smallInteger('hours_worked')->unsigned()->default(0);
            $table->text('details')->nullable();
            $table->boolean('completed')->default(false);
            $table->integer('price_cents')->unsigned();
            $table->boolean('paid')->default(false);
            $table->timestamps();

            $table->foreign('client_id')
                  ->references('id')->on('clients')
                  ->onDelete('set null')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
