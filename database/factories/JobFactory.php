<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Job::class, function (Faker $faker) {
    return [
        'date'         => $faker->date(),
        'hours_worked' => random_int(1, 100) < 50 ? random_int(0, 18) : false,
        'details'      => join("\n", $faker->sentences(random_int(1, 5))),
        'completed'    => random_int(1, 100) < 25,
        'price_cents'  => $faker->numberBetween(10000, 1000000),
        'paid'         => random_int(1, 100) < 25,
    ];
});
