<?php

use Faker\Generator as Faker;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'name'     => $faker->name,
        'address'  => $faker->streetAddress,
        'suburb'   => $faker->city,
        'postcode' => $faker->postcode,
    ];
});
