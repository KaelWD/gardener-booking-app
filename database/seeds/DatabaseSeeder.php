<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Client::class, 10)->create()->each(function(App\Client $client) {
            $jobsCount = random_int(random_int(0, 1), 3);
            if ($jobsCount) {
                $jobs = factory(App\Job::class, $jobsCount)->make();

                foreach ($jobs as $job) {
                    $client->jobs()->save($job);
                }
            }
        });
    }
}
