const fetchTimeout = 2000;

const strategies = {
    /**
     * Fetch resource from the network then cache it
     * @param event
     * @param cache
     * @returns {Promise.<response>}
     */
    async cacheableRequestFailingToCache({ event, cache }) {
        try {
            let response = await timeoutFetch(event.request);
            if (shouldCache(response)) {
                cache.put(event.request, response.clone());
            }
            return response;
        } catch (error) {
            return await cache.match(event.request) || offlineResponse();
        }
    },

    /**
     * Fetch resource from cache, falling back to the network
     * @param event
     * @param cache
     * @returns {Promise.<response>}
     */
    async cacheFailingToCacheableRequest({ event, cache }) {
        try {
            let cached = await cache.match(event.request);
            if (cached !== undefined) return cached;
            let response = await timeoutFetch(event.request);
            if (shouldCache(response)) {
                cache.put(event.request, response.clone());
            }
            return response;
        } catch (error) {
            // console.error(error);
            return errorToResponse(error);
        }
    },

    /**
     * Fetch resource from network without storing it, falling back to the cache
     * @param event
     * @param cache
     * @returns {Promise.<response>}
     */
    async requestFailingToCache({ event, cache }) {
        try {
            return await timeoutFetch(event.request);
        } catch (error) {
            return cache.match(event.request);
        }
    },

    /**
     * Fetch resource from network, and respond with 404 if offline
     * @param event
     * @returns {Promise.<response>}
     */
    async requestFailingWithNotFound({ event }) {
        try {
            return await timeoutFetch(event.request);
        } catch (error) {
            return offlineResponse();
        }
    },
};

function shouldCache(response) {
    return response.status >= 200 && response.status < 300 || response.status === 0;
}

function errorToResponse(response) {
    if (Object.prototype.toString.call(response).slice(8, -1) !== 'Response') {
        const headers = { "Content-Type": "application/json" };
        response = new Response(JSON.stringify(response), { status: 502, statusText: "Bad Gateway", headers });
    }
    return response;
}

function offlineResponse() {
    const body    = JSON.stringify({ error: "Sorry, you are offline. Please try again later.", online: false });
    const headers = { "Content-Type": "application/json" };
    return new Response(body, { status: 404, statusText: "Not Found", headers });
}

function timeoutFetch() {
    return Promise.race([
        fetch(...arguments),
        new Promise((resolve, reject) => {
            setTimeout(() => {
                reject(new Error('Request timed out'))
            }, fetchTimeout)
        })
    ]);
}

export default strategies;
