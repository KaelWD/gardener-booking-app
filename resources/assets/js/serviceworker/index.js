import registerFetchHandler from './fetchHandler';

export const manifest = (async () => {
    try {
        let res = await fetch('/mix-manifest.json');
        return await res.json();
    } catch (err) {
        console.error(`[ServiceWorker] Failed to get mix manifest: ${err.message}`);
    }
})();

self.addEventListener('install', event => {
    event.waitUntil((async () => {
        try {
            let [cache, manifest] = await Promise.all([caches.open(CACHE_NAME), this.manifest]);
            console.log(`[ServiceWorker] Cached manifest %o`, manifest);
            return cache.addAll([
                '/',
                ...Object.values(manifest),
            ]);
        } finally {
            self.skipWaiting();
        }
    })());
});

self.addEventListener('activate', event => {
    const cacheWhitelist = [CACHE_NAME];
    event.waitUntil((async () => {
        let keyList = await caches.keys();
        await Promise.all(keyList.map(key => {
            if (!cacheWhitelist.includes(key)) return caches.delete(key);
        }));
        self.clients.claim();
    })());
});

registerFetchHandler();
