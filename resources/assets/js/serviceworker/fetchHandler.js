import cacheStrategies from './cacheStrategies';

function isHttpRequest(request) {
    return /^https*/.test(request.url);
}

function isRequestForStatic(request) {
    return /.(png|jpg|jpeg|gif|css|js)$/.test(request.url);
}

function isSideEffectRequest(request) {
    return ["POST", "PUT", "DELETE"].includes(request.method);
}

function isOnlineCheckRequest(request) {
    return /\/online-check$/.test(request.url);
}

export default function registerFetchHandler() {
    self.addEventListener('fetch', event => {

        if(!isHttpRequest(event.request)) {
            console.debug('[ServiceWorker] Intercepted non-http fetch %s', event.request.url);
            event.respondWith(
                cacheStrategies.requestFailingWithNotFound({ event }),
            );
            return;
        }

        if (isSideEffectRequest(event.request)) {
            console.debug('[ServiceWorker] Intercepted SideEffect fetch %s', event.request.url);
            event.respondWith(cacheStrategies.requestFailingWithNotFound({ event }));
            return;
        }

        if (isRequestForStatic(event.request)) {
            console.debug('[ServiceWorker] Intercepted Static fetch %s', event.request.url);
            event.respondWith((async () => {
                let cache = await caches.open(CACHE_NAME);
                return cacheStrategies.cacheFailingToCacheableRequest({ event, cache });
            })());
            return;
        }

        if (isOnlineCheckRequest(event.request)) {
            console.debug('[ServiceWorker] Intercepted OnlineCheck');
            event.respondWith(
                cacheStrategies.requestFailingWithNotFound({ event }),
            );
            return;
        }

        console.debug('[ServiceWorker] Intercepted fetch %s', event.request.url);

        event.respondWith((async () => {
            let cache = await caches.open(CACHE_NAME);
            return cacheStrategies.cacheableRequestFailingToCache({ event, cache });
        })());

    });
}
