export default {
    jobs: [],
    clients: [],
    userPreferences: {
        darkTheme: false,
    },
    snackbars: [],
}
