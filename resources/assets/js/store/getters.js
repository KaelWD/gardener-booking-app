import store from './index';

export default {
    getJobById: (state, getters) => (id) => {
        return getters.getModelById('jobs', id);
    },
    getClientById: (state, getters) => (id) => {
        return getters.getModelById('clients', id);
    },
    getJobsByDate: (state, getters) => async (date) => {
        //
    },
    getModelById: (state, getters) => async (name, id) => {
        if (id == null) return;
        const PascalCaseName = name.charAt(0).toUpperCase() + name.slice(1);
        let fresh     = false;
        const refresh = async () => {
            console.debug(`[Store] 'getModelById' refreshed '%s'; fresh = %s, id = %s`, name, fresh, id);
            await store.dispatch(`refresh${PascalCaseName}`);
            fresh = true;
        };
        const search  = async () => {
            console.debug(`[Store] 'getModelById' searched '%s' for ID '%s'; fresh = %s`, name, id, fresh);
            let found = state[name].find(model => Number(model.id) === Number(id));
            if (fresh || found !== undefined) return found;
            await refresh();
            return search();
        };

        console.debug(`[Store] 'getModelById' preparing to get '%s' with ID' %s'`, name, id);

        if (!state[name].length) await refresh();

        return search();
    }
}
