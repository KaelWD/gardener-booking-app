import * as types from './mutation-types';

export default {
    [types.SET_CLIENTS](state, clients) {
        state.clients = clients;
    },
    [types.SET_CLIENT](state, client) {
        let existing = client.id && state.clients.find(e => e.id === client.id);
        if (existing) Object.assign(existing, client);
        else state.clients.push(client);
    },
    [types.SET_JOBS](state, jobs) {
        state.jobs = jobs;
    },
    [types.SET_JOB](state, job) {
        let existing = job.id && state.jobs.find(e => e.id === job.id);
        if (existing) Object.assign(existing, job);
        else state.jobs.push(job);
    },
    [types.SET_USER_PREFERENCES](state, preferences) {
        state.userPreferences = preferences;
    },
    [types.SET_USER_PREFERENCE](state, preference) {
        Object.assign(state.userPreferences, preference);
    },
    [types.ADD_SNACKBAR](state, data) {
        state.snackbars = [...state.snackbars, data];
    },
    [types.REMOVE_SNACKBAR](state) {
        state.snackbars = [...state.snackbars.slice(1, state.snackbars.length + 1)];
    }
}
