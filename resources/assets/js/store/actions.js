import {
    SET_CLIENTS,
    SET_JOBS,
    SET_JOB,
    SET_USER_PREFERENCES,
    ADD_SNACKBAR,
} from './mutation-types';
import axios from 'axios';
import { reportError } from 'js/helpers';

export default {
    async refreshClients({ commit }) {
        try {
            let clients = await axios.get('/api/clients');
            commit(SET_CLIENTS, clients.data.data);
        } catch (err) {
            throw reportError(err);
        }
    },
    async refreshJobs({ commit }) {
        try {
            let jobs = await axios.get('/api/jobs');
            commit(SET_JOBS, jobs.data.data);
        } catch (err) {
            throw reportError(err);
        }
    },
    async refreshUserPreferences({ commit }) {
        try {
            let preferences = await axios.get('/api/user/preferences');
            commit(SET_USER_PREFERENCES, preferences.data.data);
        } catch (err) {
            throw reportError(err);
        }
    },

    async addJob({ commit }, formData) {
        try {
            let newJob = await axios.post('/api/jobs', formData);
            commit(SET_JOB, newJob.data.data);

            return newJob.data.data.id;
        } catch (err) {
            throw reportError(err);
        }
    },

    async updateJob({ commit }, { job, formData }) {
        if (!job.id) throw new Error('Job does not exist yet');

        try {
            let newJob = await axios.put(`/api/jobs/${job.id}`, formData);
            commit(SET_JOB, newJob.data.data);

            return newJob.data.data;
        } catch (err) {
            throw reportError(err);
        }
    },

    snackbar({ commit }, data) {
        if (typeof data === 'string') {
            data = { message: data };
        }
        if (data.duration == null || data.duration <= 0) {
            data.duration = 4000;
        } else if (data.duration < 750) {
            throw new Error('Duration must be at least 750ms');
        }
        commit(ADD_SNACKBAR, data);
    },
}
