import './bootstrap';
import Vue from 'vue';
import Vuetify from 'vuetify';
import { sync } from 'vuex-router-sync';
import router from './router';
import store from './store';
import AsyncComputed from 'vue-async-computed'

sync(store, router);

Vue.use(Vuetify);

Vue.use(AsyncComputed);

const app = new Vue({
    el: '#app',
    data: {
        isOnline: true,
    },
    render(r) {
        return r('app');
    },
    router,
    store,
    components: {
        app: require('./app.vue'),
    },
    computed: {
        darkTheme() {
            return this.$store.state.userPreferences.darkTheme;
        },
    },
    created() {
        console.debug('[App] Created');
        this.onlineCheck();
        window.setInterval(this.onlineCheck, 15000);
        this.$store.dispatch('refreshUserPreferences');
    },
    mounted() {
        if (!('serviceWorker' in navigator)) {
            this.$store.dispatch('snackbar', {
                message: `Your device does not support ServiceWorkers. This app will not be available offline.`,
            });
        } else {
            setTimeout(() => {
                if (!window.serviceWorkerWasInstalled) {
                    this.$store.dispatch('snackbar', {
                        message: `ServiceWorker failed to install. This app will not be available offline.`,
                    }, 2000);
                }
            }, 2500);
        }
    },
    methods: {
        async onlineCheck() {
            try {
                if (document.hidden) return;
                let res = await window.axios.get('/online-check', { timeout: 5000 });
                if (res.data.online) this.isOnline = res.data.online;
            } catch (err) {
                this.isOnline = false;
                console.error(err.message);
            }
        },
    },
    watch: {
        isOnline(value) {
            if (value) this.$store.dispatch('snackbar', {
                message: `Back online`,
            });
            else this.$store.dispatch('snackbar', {
                message: `Internet connection lost`,
            })
        },
    }
});
