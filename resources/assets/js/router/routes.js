export default [
    {
        path: '/',
        component: require('js/views/Root.vue'),
        children: [
            {
                path: '',
                name: 'today',
                component: require('js/views/Today.vue'),
            },
            {
                path: 'jobs',
                name: 'jobs',
                component: require('js/views/Jobs.vue'),
                meta: {
                    fabEnabled: true,
                    fabIcon: 'add',
                    fabAction: { name: 'addJob' },
                },
                children: [
                    {
                        path: ':id/edit',
                        name: 'editJob',
                        component: require('js/views/JobDialog.vue'),
                        props: (route) => ({
                            jobId: route.params.id,
                        }),
                    },
                    {
                        path: 'add',
                        name: 'addJob',
                        component: require('js/views/JobDialog.vue'),
                        props: (route) => ({
                            verb: 'Add',
                        }),
                    },
                ],
            },
            {
                path: 'clients',
                name: 'clients',
                component: require('js/views/Clients.vue'),
                meta: {
                    fabEnabled: true,
                    fabIcon: 'add',
                    // TODO: Client form
                    // fabAction: { name: 'addClient' },
                },
                children: [
                    {
                        path: ':id',
                        name: 'viewClient',
                        component: require('js/views/ViewClient.vue'),
                        meta: {
                            fabEnabled: true,
                            fabIcon:    'edit',
                            // fabAction: { name: 'editClient' },
                        },
                        props: (route) => ({
                            clientId: route.params.id,
                        }),
                        children: [
                            {
                                path: 'edit',
                                name: 'editClient',
                                component: {},
                            }
                        ],
                    },
                    {
                        path: 'add',
                        name: 'addClient',
                        component: {},
                    },
                ],
            },
        ]
    },
    {
        path: '/settings',
        name: 'settings',
        component: require('js/views/Settings.vue'),
    },
    {
        path: '*',
        component: require('js/views/NotFound.vue'),
    },
]
