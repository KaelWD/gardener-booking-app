import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes';

Vue.use(Router);

const router = new Router({
    routes,
    mode: 'hash',
    linkActiveClass: 'is-active',
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            if (savedPosition.x !== 0 || savedPosition.y !== 0) return savedPosition;
            return;
        }
        return { x: 0, y: 0 };
    },
});

export default router;
