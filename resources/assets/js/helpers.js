import store from 'js/store';

/**
 * Insert 'el' between every element in 'arr'
 * @param {Array} arr
 * @param {*} el
 * @returns {Array}
 */
export function intersperse(arr, el) {
    return arr.reduce((acc, element, index, array) => {
        acc.push(element);

        if (index < array.length - 1) {
            acc.push(el);
        }

        return acc;
    }, [])
}

export function reportError(err) {
    if (err.response) {
        if (err.response.data.message) store.dispatch('snackbar', err.response.data.message);
        else store.dispatch('snackbar', err.toString());
        console.error('Data: %o, Status: %d, Headers: %o', err.response.data, err.response.status, err.response.headers);
    } else if (err.request) {
        console.error(err.request);
        store.dispatch('snackbar', err.toString());
    } else {
        console.error('Error: %o', err.message ? err.message : err.toString());
        store.dispatch('snackbar', err.message ? err.message : err.toString())
    }
    return err;
}
