<!doctype html>
<html lang="en-AU">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="{{ url('manifest.json') }}">

    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Gardeners-R-US">
    <meta name="apple-mobile-web-app-title" content="Gardeners-R-US">
    <meta name="theme-color" content="#3273dc">
    <meta name="msapplication-navbutton-color" content="#00d1b2">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="msapplication-starturl" content="/">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" sizes="16x16" href="favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="16x16" href="favicon.ico">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        @keyframes preloadSpinner {
            from {
                -webkit-transform : rotate(0deg);
                transform         : rotate(0deg);
            }

            to {
                -webkit-transform : rotate(359deg);
                transform         : rotate(359deg);
            }
        }

        body, html {
            margin     : 0;
            width      : 100%;
            height     : 100%;
        }

        #app[v-cloak] {
            width          : 100%;
            height         : 100%;
            display        : flex;
            flex-direction : column;
            overflow       : hidden;
        }

        .preload-spinner {
            -webkit-animation  : preloadSpinner 750ms infinite linear;
            animation          : preloadSpinner 750ms infinite linear;
            border             : 2px solid #aaa;
            border-radius      : 290486px;
            border-right-color : transparent;
            border-top-color   : transparent;
            content            : "";
            display            : block;
            font-size          : 4em;
            height             : 1em;
            position           : relative;
            width              : 1em;
            margin             : auto;
        }
    </style>

    <script defer src="{{ mix('/js/manifest.js') }}"></script>
    <script defer src="{{ mix('/js/vendor.js') }}"></script>
    <script defer src="{{ mix('/js/app.js') }}"></script>

</head>
<body>

<div id="app" v-cloak>
    <noscript>
        <div class="alert alert--warning ma-0">
            <div>
                <div>
                    <p class="title">You have JavaScript disabled or are using an outdated browser</p>
                    <p class="subheading">Please enable JavaScript or update your browser to view this page</p>
                </div>
            </div>
        </div>
    </noscript>
    <div class="preload-spinner"></div>
</div>

<link rel="stylesheet" href="{{ mix('/css/app.css') }}">

</body>
</html>
