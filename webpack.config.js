const path           = require('path');
const webpack        = require('webpack');
const Mix            = require('laravel-mix').config;
const webpackPlugins = require('laravel-mix').plugins;
const dotenv         = require('dotenv');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const BabiliPlugin = require('babili-webpack-plugin');

dotenv.config({
    path: Mix.Paths.root('.env'),
});

let config =  [
    // App bundle config
    require('./webpack.config.app'),
    {
        // ServiceWorker bundle config
        context: Mix.Paths.root(),
        entry: {
            '/sw': path.normalize(__dirname + '/resources/assets/js/serviceworker/index.js'),
        },
        output: {
            path: path.normalize(__dirname + '/public'),
            filename: '[name].js',
        },
        devtool: Mix.inProduction ? 'source-map' : 'inline-source-map',
        plugins: [
            new webpackPlugins.FriendlyErrorsWebpackPlugin({clearConsole: Mix.options.clearConsole}),
            new webpack.LoaderOptionsPlugin({
                minimize: Mix.inProduction,
                options: {
                    postcss: Mix.options.postCss,
                    context: __dirname,
                    output: {path: './'},
                },
            }),
            new webpack.optimize.CommonsChunkPlugin({
                name: '/sw',
                minChunks: Infinity,
            }),
            new webpack[Mix.inProduction ? 'HashedModuleIdsPlugin' : 'NamedModulesPlugin'](),
            new webpackPlugins.WebpackChunkHashPlugin(),
            new webpack.DefinePlugin(
                Mix.definitions({
                    NODE_ENV: Mix.inProduction
                        ? 'production'
                        : ( process.env.NODE_ENV || 'development' ),
                })
            ),
            new webpack.DefinePlugin({
                CACHE_NAME: JSON.stringify(process.env.CACHE_NAME || 'v1::'),
            }),
            new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                reportFilename: 'report-sw.html',
                openAnalyzer: false,
            }),
        ],
    },
];

if (Mix.inProduction && Mix.options.uglify) {
    config[1].plugins.push(
        new BabiliPlugin()
    );
}

config[0].entry['/js/app'].unshift('babel-polyfill');

module.exports = config;
