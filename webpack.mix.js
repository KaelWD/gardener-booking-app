const { mix }              = require('laravel-mix');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/app.js', 'public/js')
    .extract([
        'vue',
        'vuex',
        'axios',
        'vuetify',
        'vue-router',
        'babel-polyfill',
        'vuex-router-sync',
        'regenerator-runtime',
    ])
    .stylus('resources/assets/stylus/app.styl', 'public/css')
    .copyDirectory('resources/static', 'public')
    .webpackConfig({
        output: {
            chunkFilename: mix.config.inProduction ? 'js/[name].[chunkhash:20].js' : 'js/[name].js',
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.runtime.common.js',
            },
        },
        plugins: [
            new BundleAnalyzerPlugin({
                analyzerMode: 'static',
                openAnalyzer: false,
            }),
        ],
    })
    .browserSync({
        proxy: 'http://127.0.0.1:8000',
        reloadOnRestart: false,
        open: false,
    });

if (mix.config.inProduction) mix.version();
else mix.sourceMaps();
